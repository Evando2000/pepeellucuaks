[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/iaksesoris-transformasi-toko-akuakultur/badges/master/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/iaksesoris-transformasi-toko-akuakultur/-/commits/master)
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/iaksesoris-transformasi-toko-akuakultur/badges/master/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/iaksesoris-transformasi-toko-akuakultur/-/commits/master)

# iAksesoris - Backend
This is backend repository service of iAksesoris built with Django.

## Magic People - PPL C
* 1806205445 - Evando Wihalim
* 1806205653 - Michael Susanto
* 1806205483 - Alwan Harrits Surya Ihsan
* 1806205571 - Ryo Axtonlie
* 1806204985 - Jonathan

## How to run (Local)
* Clone this project
```cmd
git clone https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/iaksesoris-transformasi-toko-akuakultur.git
```

* Build & run the project
```cmd
docker-compose up -d --build
```

* Open **localhost:8000**

* Stop the project
```cmd
docker-compose down
```

* Stop the project (database volume removal)
```cmd
docker-compose down -v
```

## How to run test
```cmd
docker-compose run web coverage run manage.py test
docker-compose run web coverage report -m
```

## How to run (production)
* Build & run the project
```cmd
docker-compose -f docker-compose.prod.yml up -d --build
```

* Stop the project
```cmd
docker-compose -f docker-compose.prod.yml down
```

* Stop the project (database volume removal)
```cmd
docker-compose -f docker-compose.prod.yml down -v
```