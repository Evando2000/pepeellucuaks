#!/bin/bash
python manage.py makemigrations
python manage.py makemigrations product
python manage.py migrate
python manage.py collectstatic --no-input
gunicorn iaksesorisbe.wsgi -b 0.0.0.0:8000