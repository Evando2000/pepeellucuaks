from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

class HelloTest(TestCase):
    def test_hello_world(self):
        url = reverse('hello')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)