from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from utils.enable_cors import cors_enabled

@csrf_exempt
@cors_enabled
def hello(request):
    return JsonResponse({'response': 'Hello World'}, status=200)