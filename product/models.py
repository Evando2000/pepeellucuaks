from django.db import models

# Create your models here.
class Product(models.Model):
    product_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, null=False)
    stock = models.IntegerField(default=0)
    min_order = models.IntegerField(default=0)
    description = models.CharField(max_length=255)
    category = models.CharField(max_length=255)
    is_show = models.BooleanField(default=False)
    admin_id = models.IntegerField(default=0)

class ProductImage(models.Model):
    image_url = models.FileField(blank=True, null=True)
    product_id = models.ForeignKey(Product, related_name='images', on_delete=models.CASCADE, null=False)

class ProductPrice(models.Model):
    price = models.PositiveIntegerField(default=0)
    quantity = models.PositiveIntegerField(default=0)
    product_id = models.ForeignKey(Product, related_name='prices', on_delete=models.CASCADE, null=False)