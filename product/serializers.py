from rest_framework import serializers
from .models import Product, ProductImage, ProductPrice

class ProductImageSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ProductImage
        fields = ['id', 'image_url']

class ProductPriceSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ProductPrice
        fields = ['id', 'price', 'quantity']

class ProductSerializer(serializers.ModelSerializer):
    images = ProductImageSerializer(many=True, read_only=True)
    prices = ProductPriceSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['product_id','title','stock','min_order','description','category', 'images', 'prices']