from .apps import ProductConfig
from .models import Product, ProductImage, ProductPrice
from .serializers import ProductSerializer
from django.apps import apps
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

# Create your tests here.
class ProductConfigTest(TestCase):

    def test_appname(self):
        self.assertEqual(ProductConfig.name, 'product')
        self.assertEqual(apps.get_app_config('product').name, 'product')


class ProductTest(APITestCase):

    def setUp(self):
        self.product_1 = Product.objects.create(
            product_id=1, 
            title='test_title',
            stock = 1000,
            min_order = 100,
            description = 'test_description',
            category = 'test_category',
            is_show = True,
            admin_id = 1
        )
        ProductImage.objects.create(
            image_url = 'images/url',
            product_id = Product.objects.get(pk=1)
        )
        ProductPrice.objects.create(
            price = 18000,
            product_id = Product.objects.get(pk=1)
        )

    def test_model_product(self):
        self.assertEqual(self.product_1.product_id, 1)
        self.assertEqual(self.product_1.title, 'test_title')
        self.assertEqual(self.product_1.stock, 1000)
        self.assertEqual(self.product_1.min_order, 100)
        self.assertEqual(self.product_1.description, 'test_description')
        self.assertEqual(self.product_1.category, 'test_category')
        self.assertEqual(self.product_1.is_show, True)
        self.assertEqual(self.product_1.admin_id, 1)

    def test_model_productimage(self):
        test_productimage = ProductImage.objects.create(
            image_url = 'images/url',
            product_id = Product.objects.get(pk=1)
        )
        self.assertEqual(test_productimage.image_url, 'images/url')
        self.assertEqual(test_productimage.product_id, self.product_1)

    def test_model_productprice(self):
        test_productprice = ProductPrice.objects.create(
            price = 18000,
            product_id = Product.objects.get(pk=1)
        )
        self.assertEqual(test_productprice.price, 18000)
        self.assertEqual(test_productprice.product_id, self.product_1)
    
    def test_get_all_products(self):
        url = reverse('url_get_product')
        response = self.client.get(url, format='json')
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_get_product_detail_not_found(self):
        url = reverse('url_get_product_detail', kwargs={'product_id':2})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        
    def test_get_product_detail(self):
        url = reverse('url_get_product_detail', kwargs={'product_id':1})
        response = self.client.get(url, format='json')
        product_test = Product.objects.get(pk=self.product_1.product_id)
        serialized_product = ProductSerializer(product_test)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serialized_product.data)