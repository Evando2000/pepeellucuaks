from django.urls import path, include
from .views import *

urlpatterns = [    
    path('product/', ProductView.as_view(), name='url_get_product'),
    path('product/<int:product_id>', ProductDetailView.as_view(), name='url_get_product_detail'),
]