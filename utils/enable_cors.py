from django.http import HttpResponse


def cors_enabled(func):
    def inner_func(request, *args, **kwargs):
        if request.method == "OPTIONS":
            val = HttpResponse()
        else:
            val = func(request, *args, **kwargs)
        val["Access-Control-Allow-Origin"] = "*"
        val["Access-Control-Allow-Methods"] = "GET, POST, PATCH, PUT, DELETE"
        val["Access-Control-Allow-Headers"] = "Origin, Content-Type, X-Auth-Token"
        return val

    return inner_func
